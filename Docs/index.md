<!-- Physics 521: Classical Mechanics I documentation master file.-->

<!-- Literally include the README.md file -->
```{include} README.md
```

## Contents

```{toctree}
---
maxdepth: 2
titlesonly:
---
Syllabus
Reading
Assignments
References
```
